var React = require('react');
var classNames = require('classnames');

module.exports = React.createClass({

  propTypes: {
    type: React.PropTypes.string.isRequired,
    enabled: React.PropTypes.bool.isRequired,
    click: React.PropTypes.func.isRequired
  },
  getDefaultProps: function() {
    return {
      type: '',
      click: function(){},
      enabled: false
    }
  },

  clickHandler: function(evt) {
    evt.preventDefault();
    if(!this.props.enabled) return;

    this.props.click(evt);
  },

  render: function() {
    var btnStyles = classNames(
      'nav-button', this.props.type, {
      'disabled': !this.props.enabled
    });
    var iconStyles = classNames(
      'glyphicon', 'glyphicon-chevron-' + this.props.type
    );
    return <button className={btnStyles} onClick={this.clickHandler}>
      <span className={iconStyles} />
    </button>;
  }

});
