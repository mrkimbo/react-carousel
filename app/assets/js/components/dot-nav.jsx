var React = require('react');
var classNames = require('classnames');

module.exports = React.createClass({

  propTypes:  {
    count: React.PropTypes.number.isRequired,
    index: React.PropTypes.number.isRequired
  },

  getDefaultProps: function () {
    return {
      count: 0,
      index: 0
    };
  },

  generateDot(item, idx) {
    var styles = classNames('dot', {
      'selected': this.props.index === idx
    });
    var key = 'dot-nav-dot-' + idx;
    return <li key={key} className={styles}></li>;
  },

  render: function () {
    var range = new Array(this.props.count).fill(false);

    return <div className="dot-nav">
      <ul>
        {range.map(this.generateDot)}
      </ul>
    </div>;
  }

});
