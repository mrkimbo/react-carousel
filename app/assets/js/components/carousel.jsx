var React = require('react');

var ImgContainer = require('./image-container.jsx'),
  DotNav = require('./dot-nav.jsx'),
  NavButton = require('./nav-button.jsx');

var LEFT = 'left',
  RIGHT = 'right';

module.exports = React.createClass({
  propTypes: {
    items: React.PropTypes.array.isRequired,
    wrap: React.PropTypes.bool.isRequired
  },
  getInitialProps: function () {
    return {
      items: [],
      wrap: false
    }
  },
  getInitialState: function () {
    return {
      index: 0
    }
  },
  componentDidMount: function () {
    // assume chrome
    if (!this.imgContainerRef) {
      this.imgContainerRef = this.getDOMNode()
        .querySelector('.image-container');
    }

    this.imgContainerRef.addEventListener(
      'webkitTransitionEnd', this.onTransitionEnd
    );
  },
  componentDidUnmount: function () {
    this.imgContainerRef.removeEventListener(
      'webkitTransitionEnd', this.onTransitionEnd
    );
  },


  prev: function () {
    var idx = this.state.index - 1;
    if (this.props.wrap && idx < 0) idx = this.props.items.length - 1;
    this.setState({
      index: idx,
      locked: true
    });
  },
  next: function () {
    var idx = this.state.index + 1;
    if (this.props.wrap && idx === this.props.items.length) idx = 0;
    this.setState({
      index: idx,
      locked: true
    });
  },
  onTransitionEnd: function () {
    this.setState({
      locked: false
    })
  },
  getEnabled: function (type) {
    if (this.state.locked) return false;
    if (this.props.wrap) return true;

    switch (type.toLowerCase()) {
      case LEFT:
        return this.state.index > 0;
      case RIGHT:
        return this.state.index < this.props.items.length - 1;
      default:
        return false;
    }
  },
  generateImage: function (item, idx) {
    return <img key={idx} src={item.img}/>;
  },

  getImageCaption: function () {
    return this.props.items[this.state.index].caption;
  },

  render: function () {
    return (
      <div className="carousel">
        <ImgContainer items={this.props.items} index={this.state.index}/>

        <div className="caption"><h3>{this.getImageCaption()}</h3></div>
        <div className="controls">
          <NavButton type={LEFT} click={this.prev} enabled={this.getEnabled(LEFT)}/>
          <NavButton type={RIGHT} click={this.next} enabled={this.getEnabled(RIGHT)}/>
        </div>
        <DotNav count={this.props.items.length} index={this.state.index}/>
      </div>
    )
  }

});
