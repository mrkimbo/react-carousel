var React = require('react'),
  ReactTransition = require('react/lib/ReactCSSTransitionGroup');

module.exports = React.createClass({

  propTypes: {
    items: React.PropTypes.array.isRequired,
    index: React.PropTypes.number.isRequired
  },
  getDefaultProps: function () {
    return {
      items: [],
      index: 0
    }
  },
  getInitialState: function () {
    return {
      itemWidth: window.screen.width
    }
  },

  generateImage: function (item, idx) {
    var style = {width: this.state.itemWidth};
    return <img key={idx} src={item.img} style={style}/>
  },

  componentDidMount: function () {
    var width = parseFloat(
      window.getComputedStyle(this.getDOMNode().parentNode).width
    );
    this.setState({
      itemWidth: width
    });
  },

  getStyle: function () {
    return {
      width: this.state.itemWidth * this.props.items.length,
      left: this.state.itemWidth * this.props.index * -1
    };
  },

  render: function () {
    return <div className="image-container" style={this.getStyle()}>
        {this.props.items.map(this.generateImage)}
    </div>;
  }
});
